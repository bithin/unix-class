#!/bin/sh
#
#Script to test while statement
#
#
if [ $# -eq 0 ]
then
    echo "You have to enter a number"
    exit 1
fi
n=$1
i=1
while [ $i -le 10 ]
do
    echo "$n * $i = `expr $i \* $n`"
    i=`expr $i + 1`
done
