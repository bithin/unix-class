#!/bin/bash
student_db()
{
    recno=0
    clear
    echo "Student Database Management System"
    echo -n "Enter the student record file name : "
    read filename
    if [ ! -f $filename ]
    then
        echo "Sorry, $filename does not exit, Creating $filename database"
        echo "Student Record" > $filename
    fi
    echo "Record Created On Date: `date`" >/tmp/input0.$$
    
    while :
    do
        echo -n "Student Name:"
        read name
        echo -n "Roll No:"
        read roll
        echo -n "CGPA:"
        read cgpa
        echo -n "Is data okay (y/n) ?"
        read ans
        if [ $ans = y -o $ans = Y ]
        then
            recno=`expr $recno + 1`
            echo "$recno. $name $roll $cgpa" >> /tmp/input0.$$
        fi
        echo -n "Add next student (y/n)?"
        read isnext
        if [ $isnext = n -o $isnext = N ]
        then
            cat /tmp/input0.$$ >> $filename
            rm -f /tmp/input0.$$
            return 
        fi
    done
}

student_db
